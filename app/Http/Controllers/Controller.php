<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function sentSuccessResponse($data, $message = 'success', $status = Response::HTTP_OK)
    {
        if ($message == '') {
            return response()->json([
                'data' => $data
            ], $status);
        }

        return response()->json([
            'data' => $data,
            'message' => $message
        ], $status);
    }
}
