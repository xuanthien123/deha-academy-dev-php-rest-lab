<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreAndUpdatePostRequest;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProductController extends Controller
{

    protected $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $product = $this->product->paginate(5);

        //$productResource = ProductResource::collection($product)->response()->getData(true);

        $productCollection = new ProductCollection($product);

        return $this->sentSuccessResponse($productCollection,'success',Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreProductRequest $request)
    {
        $dataCreate = $request->all();
        $product = $this->product->create($dataCreate);

        $productResource = new ProductResource($product);

        return $this->sentSuccessResponse($productResource,'success',Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $product = $this->product->findOrFail($id);

        $productResource = new ProductResource($product);

        return $this->sentSuccessResponse($productResource,'success',Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateProductRequest $request, $id)
    {
        $product = $this->product->findOrFail($id);
        $dataUpdate = $request->all();

        $product->update($dataUpdate);

        $productResource = new ProductResource($product);

        return $this->sentSuccessResponse($productResource,'success',Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $product = $this->product->findOrFail($id);
        $product->delete();
        $productResource = new ProductResource($product);

        return $this->sentSuccessResponse($productResource,'success',Response::HTTP_OK);
    }
}
