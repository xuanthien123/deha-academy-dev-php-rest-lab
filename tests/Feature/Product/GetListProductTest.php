<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class GetListProductTest extends TestCase
{
    /** @test */
    public function user_can_get_list_products()
    {
        $productCount = Product::count();
        $response = $this->getJson(route('products.index'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json) =>
            $json->has('data',fn(AssertableJson $json) =>
                $json->has('data')
                    ->has('meta',fn(AssertableJson $json) =>
                        $json->where('total',$productCount)
                    )
            )
            ->etc()
        );

    }
}
