<?php

namespace Tests\Feature\Posts;

use App\Models\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class UpdatePostTest extends TestCase
{
    /** @test */

    public function user_can_update_if_post_exists_and_data_is_valid()
    {
        $post = Post::factory()->create();
        $dataCreate = [
            'name' => $this->faker->name,
            'body' => $this->faker->text
        ];

        $response = $this->json('put',route('posts.update', $post->id),$dataCreate);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json)=>
            $json->has('message')
            ->has('data',fn(AssertableJson $json)=>
                $json->where('name', $dataCreate['name'])
                ->where('id', $post->id)
            )
        );
        $this->assertDatabaseHas('posts',[
            'id' => $post->id,
            'name' => $dataCreate['name'],
            'body' => $dataCreate['body']
        ]);


    }

    /** @test */
    public function user_can_not_update_if_post_exists_and_name_is_null()
    {
        $post = Post::factory()->create();
        $dataCreate = [
            'name' => '',
            'body' => $this->faker->text
        ];

        $response = $this->json('put',route('posts.update', $post->id),$dataCreate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json)=>
            $json->has('errors',fn(AssertableJson $json)=>
                $json->has('name')
            )
        );
    }

    /** @test */
    public function user_can_not_update_if_post_exists_and_body_is_null()
    {
        $post = Post::factory()->create();
        $dataCreate = [
            'name' => $this->faker->name,
            'body' => ''
        ];

        $response = $this->json('put',route('posts.update', $post->id),$dataCreate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json)=>
            $json->has('errors',fn(AssertableJson $json)=>
                $json->has('body')
            )
        );
    }

    /** @test */
    public function user_can_not_update_if_post_exists_and_data_is_not_valid()
    {
        $post = Post::factory()->create();
        $dataCreate = [
            'name' => '',
            'body' => ''
        ];

        $response = $this->json('put',route('posts.update', $post->id),$dataCreate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json)=>
            $json->has('errors',fn(AssertableJson $json)=>
                $json->has('body')
                    ->has('name')
            )
        );
    }

    /** @test */
    public function user_can_not_update_if_post_not_exists_and_data_is_valid()
    {
        $postId = -1;
        $dataCreate = [
            'name' => $this->faker->name,
            'body' => $this->faker->text
        ];

        $response = $this->json('put',route('posts.update', $postId),$dataCreate);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $response->assertJson(fn(AssertableJson $json)=>
            $json->where('status',Response::HTTP_NOT_FOUND)
                ->has('message')
        );
    }
}
